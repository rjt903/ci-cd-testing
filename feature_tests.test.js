
const puppeteer = require('puppeteer');

describe('Homepage Feature Tests', () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  afterAll(async () => {
    await browser.close();
  });

  test('Clicking the button fetches data and updates the page', async () => {
    // Replace with the actual URL or file path
    const url = 'http://louis-robert-bucket2.s3-website.eu-west-2.amazonaws.com/';

    await page.goto(url);

    const buttonSelector = 'button';
    const responseSelector = '#response';
    const imgSelector = '#img';

    // Click the button
    await page.click(buttonSelector);

    // Wait for the response to be updated
    await page.waitForSelector(responseSelector);

    // Get the updated text
    const responseText = await page.$eval(responseSelector, (el) => el.textContent.trim());

    // Assert that the response text is what you expect
    expect(responseText).toContain('Louis & Robert! You have successfully invoked your first Lambda. Great job!');

    // Get the source attribute of the image
    const imgSrc = await page.$eval(imgSelector, (el) => el.src);

    // Assert that the image source is what you expect
    expect(imgSrc).toContain('https://images.unsplash.com/photo-1540573133985-87b6da6d54a9');
  });
});
