import json

def lambda_handler(event, context):
    print("Inside the lambda handler")

    response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*",
        },
        "body": "Louis & Robert! You have successfully invoked your first Lambda...again! Great job!"
    }
    print(response)

    return response
